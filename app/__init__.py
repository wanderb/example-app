from flask import (
  Blueprint,
  flash,
  Flask,
  g,
  render_template,
  request,
  session
)

from kubernetes import client, config
import socket
import yaml

def create_app():
  app=Flask(__name__)

  @app.route("/")
  def home():
    config.load_incluster_config()
    hostname = socket.gethostname()

    v1 = client.CoreV1Api()
    ns = open('/var/run/secrets/kubernetes.io/serviceaccount/namespace','r').read()

    pods = v1.list_namespaced_pod(ns)

    ourpod = ''

    for pod in pods.items:
      if pod.metadata.name == hostname:
        ourpod = yaml.dump(pod.to_dict())


    return render_template("helloworld.html", ourpod=ourpod, hostname=hostname)
  
  @app.route("/doei")
  def doei():
    return "Doei"

  return app
